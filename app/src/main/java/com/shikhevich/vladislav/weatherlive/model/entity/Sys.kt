package com.shikhevich.vladislav.weatherlive.model.entity

data class Sys(
    var pod: String = ""
)