package com.shikhevich.vladislav.weatherlive.utils

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.shikhevich.vladislav.weatherlive.R

class MyLocationManager(
    private val context: Context,
    private val completionCoordinates: (Double, Double) -> Unit,
    private val completionLoading: (Boolean) -> Unit,
    private val completionMessage: (String) -> Unit)
    : GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

    private val interval = 3000L

    private val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private var locationRequest: LocationRequest? = null
    private var location: Location? = null

    private var googleApiClient: GoogleApiClient = GoogleApiClient.Builder(context)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build()

    fun locate() {
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            googleApiClient.connect()
        }
        else {
            context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }

    fun disconnect() {
        if(googleApiClient.isConnected)
            googleApiClient.disconnect()
    }

    private fun startLocate() {
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(interval)
            .setFastestInterval(interval)

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    override fun onConnected(p0: Bundle?) {
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        startLocate()
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)

        if(location != null) {
            completionCoordinates(location!!.latitude, location!!.longitude)
        }
        else {
            completionMessage(context.getString(R.string.couldNotDetermineLocation))
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        googleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        completionLoading(false)
        completionMessage(p0.errorMessage!!)
    }

    override fun onLocationChanged(p0: Location?) {}
}