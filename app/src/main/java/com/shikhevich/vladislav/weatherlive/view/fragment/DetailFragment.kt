package com.shikhevich.vladislav.weatherlive.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shikhevich.vladislav.weatherlive.databinding.FragmentDetailBinding

class DetailFragment
    : Fragment() {

    companion object {
        fun getInstance(position: Int) : DetailFragment {
            val detailFragment = DetailFragment()
            val args = Bundle()
            args.putInt(KEY_POSITION, position)
            detailFragment.arguments = args
            return detailFragment
        }

        const val KEY_POSITION = "_position"
    }

    private lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchViewModel = ViewModelProviders.of(activity!!).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentDetailBinding = FragmentDetailBinding.inflate(inflater, container, false)
        val view = fragmentDetailBinding.root

        val wm = searchViewModel.getWeatherModel().value!!
        val position = arguments!!.getInt(KEY_POSITION, 0)

        fragmentDetailBinding.wm = wm
        fragmentDetailBinding.position = position
        fragmentDetailBinding.imageUrl =
            "http://openweathermap.org/img/w/${wm.list[position].weather[0].icon}.png"

        return view
    }
}