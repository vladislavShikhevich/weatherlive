package com.shikhevich.vladislav.weatherlive.db

import android.arch.persistence.room.Room
import android.content.Context
import android.util.Log
import com.shikhevich.vladislav.weatherlive.db.iface.OnDatabaseAddListener
import com.shikhevich.vladislav.weatherlive.db.iface.OnDatabaseLoadListener
import com.shikhevich.vladislav.weatherlive.model.entity.*
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/*
Пришлось вручную доставать из бд списки и присваивать их weatherModel,
так как конвертеры у меня не работали, а почему я не успел разобраться,
да и делать список списков в одной таблице, возможно, не самое лучшее решение
*/
class LocalCacheManager private constructor(context: Context){

    companion object {

        private val TAG = LocalCacheManager::class.java.simpleName

        private const val DB_NAME = "database"

        private var instance: LocalCacheManager? = null

        fun getInstance(context: Context) : LocalCacheManager? {
            if(instance == null)
                instance = LocalCacheManager(context)
            return instance
        }
    }

    private var db: AppDatabase

    init {
        db = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME).build()
    }

    private fun getWeatherModel(xs: List<X>, onDatabaseCallbacks: OnDatabaseLoadListener) {
        db.weatherModelDao().getById(1).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { t ->
            t.list = xs
            onDatabaseCallbacks.onWeatherModelLoaded(t)
        }
    }

    fun addWeatherModel(onDatabaseAddListener: OnDatabaseAddListener, weatherModel: WeatherModel) {
        Completable.fromAction {
            db.weatherModelDao().insert(weatherModel)

            /*Пришлось сделать запись в цикле, так как изначально с REST API id не приходят, а для работы с бд они нужны,
            поэтому пробегаю в цикле и вручную присваиваю id, другого решения пока не успел придумать*/
            for(i in weatherModel.list.indices) {
                val x = weatherModel.list[i]
                weatherModel.list[i].idX = i + 1
                x.weather[0].idWeather = i + 1
                addWeathers(x.weather)
            }

            addXs(weatherModel.list)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onComplete() {
                    onDatabaseAddListener.onWeatherModelAdded(weatherModel)
                }

                override fun onError(e: Throwable) {
                    onDatabaseAddListener.onDataNotAvailable(Error.UNKNOWN)
                }
            })
    }

    private fun getX(weathers: List<Weather>, onDatabaseCallbacks: OnDatabaseLoadListener) {
        db.xDao().getAll().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { t ->
            for (i in t!!.indices)
                t[i].weather = arrayListOf(weathers[i])

            getWeatherModel(t!!, onDatabaseCallbacks)
        }
    }

    private fun addXs(xs: List<X>) {
        db.xDao().insertAll(xs)
    }

    /*Сделал так потому, что, если вызывать изначально метод getWeatherModel,
    то колбек срабатывает раньше, а остальные данные не успевают подгрузиться,
    поэтому загружаю с конца и тольно потом вызываю метод колбека*/
    fun getWeather(onDatabaseCallbacks: OnDatabaseLoadListener) {
        db.weatherDao().getAll().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { t ->
            if(t.isEmpty())
                onDatabaseCallbacks.onDataNotAvailable(Error.NO_DATA)
            else
                getX(t, onDatabaseCallbacks) }
    }

    private fun addWeathers(weathers: List<Weather>) {
        db.weatherDao().insertAll(weathers)
    }
}