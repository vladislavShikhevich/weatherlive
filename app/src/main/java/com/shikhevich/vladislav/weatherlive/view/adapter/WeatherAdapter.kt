package com.shikhevich.vladislav.weatherlive.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.databinding.ItemWeatherBinding
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel
import com.shikhevich.vladislav.weatherlive.model.entity.X

class WeatherAdapter(
    private val weathers: WeatherModel,
    private val onWeatherClickListener: OnWeatherClickListener)
    : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    interface OnWeatherClickListener {
        fun onWeatherClick(x: X, position: Int)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): WeatherViewHolder
            = WeatherViewHolder(DataBindingUtil.inflate(LayoutInflater.from(p0.context), R.layout.item_weather, p0, false))

    override fun getItemCount() = weathers.list.size

    override fun onBindViewHolder(p0: WeatherViewHolder, p1: Int) {
        p0.bind(weathers.list[p1])
        p0.itemView.setOnClickListener { onWeatherClickListener.onWeatherClick(weathers.list[p1], p1) }
    }

    inner class WeatherViewHolder(private val binding: ItemWeatherBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(x: X) {
            binding.x = x
            binding.imageUrl = "http://openweathermap.org/img/w/${x.weather[0].icon}.png"
        }
    }
}