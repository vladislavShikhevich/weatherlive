package com.shikhevich.vladislav.weatherlive.model.repository

import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.app.App
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class WeatherModelRepository private constructor() {

    companion object {
        private var instance: WeatherModelRepository? = null

        fun getInstance() : WeatherModelRepository? {
            if(instance == null) {
                synchronized(WeatherModelRepository) {
                    instance = WeatherModelRepository()
                }
            }
            return instance
        }
    }

    fun getWeatherModel(
        single: Single<WeatherModel>,
        completionWeatherModel: (WeatherModel?) -> Unit,
        completionBoolean: (Boolean) -> Unit,
        completionMessage: (String) -> Unit) {

        single
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableSingleObserver<WeatherModel>() {

                override fun onSuccess(t: WeatherModel) {
                    completionWeatherModel(t)
                }

                override fun onError(e: Throwable) {
                    completionMessage(App.getInstance()!!.getString(R.string.cityNotFound))
                    completionBoolean(false)
                }
            })
    }
}