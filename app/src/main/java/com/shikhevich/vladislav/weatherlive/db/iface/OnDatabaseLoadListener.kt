package com.shikhevich.vladislav.weatherlive.db.iface

import com.shikhevich.vladislav.weatherlive.model.entity.Error
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel

interface OnDatabaseLoadListener {

    fun onWeatherModelLoaded(weatherModel: WeatherModel)

    fun onDataNotAvailable(error: Error)
}