package com.shikhevich.vladislav.weatherlive.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Weather(
    var description: String = "",
    var icon: String = "",
    var id: Int = 0,
    var main: String = "",
    @PrimaryKey(autoGenerate = true) var idWeather: Int = 1
)