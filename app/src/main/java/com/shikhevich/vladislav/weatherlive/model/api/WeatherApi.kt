package com.shikhevich.vladislav.weatherlive.model.api

import com.shikhevich.vladislav.weatherlive.model.api.WeatherApi.Consts.API_KEY
import com.shikhevich.vladislav.weatherlive.model.api.WeatherApi.Consts.DEF_CITY
import com.shikhevich.vladislav.weatherlive.model.api.WeatherApi.Consts.DEF_UNITS
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    object Consts {
        const val API_VERSION = 2.5
        const val API_KEY = "42dbaa617ac60ea9936ecf0eded541c3"
        const val DEF_API = "https://api.openweathermap.org/data/$API_VERSION/"
        const val DEF_UNITS = "metric"
        const val DEF_CITY = "Samara"
    }

    @GET("forecast")
    fun getWeatherByCityName(@Query("q") city: String = DEF_CITY,
                             @Query("units") units: String = DEF_UNITS,
                             @Query("APPID") appId: String = API_KEY)
            : Single<WeatherModel>

    @GET("forecast")
    fun getWeatherByCoordinates(@Query("lat") lat: Double,
                                @Query("lon") lon: Double,
                                @Query("units") units: String = DEF_UNITS,
                                @Query("APPID") appId: String = API_KEY)
            : Single<WeatherModel>
}