package com.shikhevich.vladislav.weatherlive.model.entity

data class Coord(
    var lat: Double = 1.0,
    var lon: Double = 1.0
)