package com.shikhevich.vladislav.weatherlive.db.dao

import android.arch.persistence.room.*
import com.shikhevich.vladislav.weatherlive.model.entity.Weather
import io.reactivex.Maybe

@Dao
interface WeatherDao {

    @Query("SELECT * FROM Weather")
    fun getAll() : Maybe<List<Weather>>

    @Query("SELECT * FROM Weather WHERE idWeather = :id")
    fun getById(id: Int) : Maybe<Weather>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weather: Weather)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(weathers: List<Weather>)

    @Update
    fun update(weather: Weather)

    @Delete
    fun delete(weather: Weather) : Int

    @Query("DELETE FROM Weather")
    fun clearAll()
}