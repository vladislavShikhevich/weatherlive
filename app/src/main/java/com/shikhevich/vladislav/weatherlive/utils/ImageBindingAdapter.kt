package com.shikhevich.vladislav.weatherlive.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shikhevich.vladislav.weatherlive.R

object ImageBindingAdapter {
    @JvmStatic
    @BindingAdapter("profileImage")
    fun setImageUrl(view: ImageView, url: String) {
        Glide
            .with(view.context)
            .setDefaultRequestOptions(RequestOptions().error(R.drawable.not_found))
            .load(url)
            .into(view)
    }
}