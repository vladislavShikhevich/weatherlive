package com.shikhevich.vladislav.weatherlive.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.view.fragment.SearchFragment
import com.shikhevich.vladislav.weatherlive.view.fragment.SearchViewModel

class MainActivity
    : AppCompatActivity() {

    private lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null) setFragment(SearchFragment.getInstance())

        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
    }

    fun setFragment(fragment: Fragment) {
        val manager = supportFragmentManager
        val curFragment = manager.findFragmentById(R.id.container)

        if(curFragment == null)
            manager
                .beginTransaction()
                .add(R.id.container, fragment)
                .commit()
        else
            manager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit()
    }
}
