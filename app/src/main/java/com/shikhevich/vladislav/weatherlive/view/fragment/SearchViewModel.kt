package com.shikhevich.vladislav.weatherlive.view.fragment

import android.app.Application
import android.arch.lifecycle.*
import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.app.App
import com.shikhevich.vladislav.weatherlive.db.iface.OnDatabaseAddListener
import com.shikhevich.vladislav.weatherlive.db.iface.OnDatabaseLoadListener
import com.shikhevich.vladislav.weatherlive.model.api.WeatherApiFactory
import com.shikhevich.vladislav.weatherlive.model.entity.Error
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel
import com.shikhevich.vladislav.weatherlive.model.repository.WeatherModelRepository
import com.shikhevich.vladislav.weatherlive.utils.MyLocationManager

class SearchViewModel(application: Application)
    : AndroidViewModel(application),
    LifecycleObserver,
    OnDatabaseLoadListener,
    OnDatabaseAddListener {

    private var weatherModel: MutableLiveData<WeatherModel> = MutableLiveData()

    private var weatherModelRepository = WeatherModelRepository.getInstance()

    private var locationManager: MyLocationManager = MyLocationManager(
        application.applicationContext,
        { lat, lon -> findWeatherModelByCoordinates(lat, lon) },
        { bool -> isLoading.value = bool },
        { msg -> message.value = msg })

    private var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    private var message: MutableLiveData<String> = MutableLiveData()

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        locationManager.disconnect()
    }

    fun getWeatherModel() = weatherModel

    fun isLoading() : LiveData<Boolean> {
        return isLoading
    }

    fun getMessage() : LiveData<String> {
        return message
    }

    fun loadWeatherModelByCityName(city: String?) {
        weatherModelRepository!!.getWeatherModel(WeatherApiFactory.build().getWeatherByCityName(city!!),
            { wm -> saveWeatherModel(wm!!) },
            { b -> isLoading.value = b },
            { msg -> message.value = msg })
    }

    fun loadWeatherModelFromDB() {
        App.getInstance()!!.getLocalCacheManager().getWeather(this)
    }

    fun loadWeatherModelByCoordinates() {
        locationManager.locate()
    }

    private fun findWeatherModelByCoordinates(lat: Double, lon: Double) {
        weatherModelRepository!!.getWeatherModel(WeatherApiFactory.build().getWeatherByCoordinates(lat, lon),
            { wm -> saveWeatherModel(wm!!) },
            { bool -> isLoading.value = bool },
            { msg -> message.value = msg })
    }

    private fun saveWeatherModel(weatherModel: WeatherModel) {
        App.getInstance()!!.getLocalCacheManager().addWeatherModel(this, weatherModel)
    }

    override fun onDataNotAvailable(error: Error) {
        when(error) {
            Error.NO_DATA -> message.value = App.getInstance()!!.getString(R.string.noData)
            Error.UNKNOWN -> message.value = App.getInstance()!!.getString(R.string.somethingWentWrong)
        }

        isLoading.value = false
    }

    override fun onWeatherModelLoaded(weatherModel: WeatherModel) {
        this.weatherModel.value = weatherModel
        isLoading.value = false
    }

    override fun onWeatherModelAdded(weatherModel: WeatherModel) {
        this.weatherModel.value = weatherModel
        isLoading.value = false
    }
}