package com.shikhevich.vladislav.weatherlive.model.entity

enum class Error {
    NO_DATA,
    UNKNOWN
}