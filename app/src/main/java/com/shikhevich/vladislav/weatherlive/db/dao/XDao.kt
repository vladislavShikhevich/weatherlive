package com.shikhevich.vladislav.weatherlive.db.dao

import android.arch.persistence.room.*
import com.shikhevich.vladislav.weatherlive.model.entity.X
import io.reactivex.Maybe

@Dao
interface XDao {

    @Query("SELECT * FROM X")
    fun getAll() : Maybe<List<X>>

    @Query("SELECT * FROM X WHERE idX = :id")
    fun getById(id: Int) : Maybe<X>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(x: X)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(weathers: List<X>)

    @Update
    fun update(x: X)

    @Delete
    fun delete(x: X) : Int

    @Query("DELETE FROM X")
    fun clearAll()
}