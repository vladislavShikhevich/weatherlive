package com.shikhevich.vladislav.weatherlive.model.entity

data class Main(
    var grnd_level: Double = 1.0,
    var humidity: Int = 0,
    var pressure: Double = 1.0,
    var sea_level: Double = 1.0,
    var temp: Double = 1.0,
    var temp_kf: Double = 1.0,
    var temp_max: Double = 1.0,
    var temp_min: Double = 1.0
)