package com.shikhevich.vladislav.weatherlive.model.entity

data class Wind(
    var deg: Double = 1.0,
    var speed: Double = 1.0
)