package com.shikhevich.vladislav.weatherlive.view.fragment

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.app.App
import com.shikhevich.vladislav.weatherlive.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment
    : Fragment() {

    companion object {
        fun getInstance() = SearchFragment()

        val TAG = SearchFragment::class.java.simpleName

        const val REQUEST_LOCATION = 1001
    }

    private lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchViewModel = ViewModelProviders.of(activity!!).get(SearchViewModel::class.java)
        lifecycle.addObserver(searchViewModel)

        searchViewModel.isLoading().observe(this,
            Observer<Boolean> { t -> showProgress(t!!) })

        searchViewModel.getMessage().observe(this,
            Observer<String> { t -> showToast(t!!) })

        searchViewModel.getWeatherModel().observe(this,
            Observer { startList() })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_search.setOnClickListener { onSearchClick() }
        btn_locate.setOnClickListener { onLocateClick() }
    }

    private fun showProgress(value: Boolean) {
        ll_search.visibility = if(value) View.GONE else View.VISIBLE
        pb_search.visibility = if(value) View.VISIBLE else View.GONE
    }

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun startList() {
        (activity as MainActivity).setFragment(ListFragment.getInstance())
    }

    private fun onSearchClick() {
        if(!edt_search.text.isNullOrEmpty()) {
            if(App.getInstance()!!.hasConnection())
                searchViewModel.loadWeatherModelByCityName(edt_search.text.toString())
            else
                searchViewModel.loadWeatherModelFromDB()
            showProgress(true)
        }
        else
            edt_search.error = getString(R.string.emptyField)
    }

    private fun onLocateClick() {
        if(ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_LOCATION)
        }
        else {
            if((context!!.getSystemService(Context.LOCATION_SERVICE) as (LocationManager)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if(App.getInstance()!!.hasConnection())
                    searchViewModel.loadWeatherModelByCoordinates()
                else
                    searchViewModel.loadWeatherModelFromDB()
                showProgress(true)
            }
            else {
                startActivity(Intent(ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }
    }
}