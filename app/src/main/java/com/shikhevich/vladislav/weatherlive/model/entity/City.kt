package com.shikhevich.vladislav.weatherlive.model.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

data class City(
    @Embedded var coord: Coord = Coord(),
    var country: String = "",
    var id: Int = 0,
    var name: String = "",
    var population: Int = 0,
    var timezone: Int = 0
)