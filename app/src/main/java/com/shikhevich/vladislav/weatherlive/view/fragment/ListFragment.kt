package com.shikhevich.vladislav.weatherlive.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shikhevich.vladislav.weatherlive.R
import com.shikhevich.vladislav.weatherlive.model.entity.X
import com.shikhevich.vladislav.weatherlive.view.activity.MainActivity
import com.shikhevich.vladislav.weatherlive.view.adapter.WeatherAdapter
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment
    : Fragment(),
    WeatherAdapter.OnWeatherClickListener {

    companion object {
        fun getInstance() = ListFragment()
    }

    private lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchViewModel = ViewModelProviders.of(activity!!).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Ради одного параметра подумал, что нет смысла использовать databinding
        txt_city.text = searchViewModel.getWeatherModel().value!!.city.name

        rv_weather.layoutManager = LinearLayoutManager(context)
        rv_weather.adapter = WeatherAdapter(searchViewModel.getWeatherModel().value!!, this)
    }

    override fun onWeatherClick(x: X, position: Int) {
        (activity as MainActivity).setFragment(DetailFragment.getInstance(position))
    }
}