package com.shikhevich.vladislav.weatherlive.app

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.net.ConnectivityManager
import com.shikhevich.vladislav.weatherlive.db.AppDatabase
import com.shikhevich.vladislav.weatherlive.db.LocalCacheManager

class App
    : Application() {

    companion object {
        private var instance: App? = null

        fun getInstance() = instance
    }

    private lateinit var localCacheManager: LocalCacheManager

    override fun onCreate() {
        super.onCreate()

        instance = this
        localCacheManager = LocalCacheManager.getInstance(this)!!
    }

    fun hasConnection() : Boolean {
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        if(wifiInfo != null && wifiInfo.isConnected)
            return true
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if(wifiInfo != null && wifiInfo.isConnected)
            return true
        wifiInfo = cm.activeNetworkInfo
        if(wifiInfo != null && wifiInfo.isConnected)
            return true
        return false
    }

    fun getLocalCacheManager() = localCacheManager
}