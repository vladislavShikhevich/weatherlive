package com.shikhevich.vladislav.weatherlive.model.entity

import android.arch.persistence.room.*

@Entity
data class WeatherModel(
    @Embedded var city: City = City(),
    var cnt: Int = 0,
    var cod: String = "",
    @Ignore var list: List<X> = ArrayList(),
    var message: Double = 1.0,
    @PrimaryKey(autoGenerate = true) var idWeatherModel: Int = 1
)