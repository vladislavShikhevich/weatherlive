package com.shikhevich.vladislav.weatherlive.model.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class WeatherApiFactory {

    companion object {
        fun build() : WeatherApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(WeatherApi.Consts.DEF_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            return retrofit.create(WeatherApi::class.java)
        }
    }
}