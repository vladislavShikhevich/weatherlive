package com.shikhevich.vladislav.weatherlive.db.dao

import android.arch.persistence.room.*
import com.shikhevich.vladislav.weatherlive.model.entity.WeatherModel
import io.reactivex.Maybe

@Dao
interface WeatherModelDao {

    @Query("SELECT * FROM WeatherModel")
    fun getAll() : Maybe<List<WeatherModel>>

    @Query("SELECT * FROM WeatherModel WHERE idWeatherModel = :id")
    fun getById(id: Int) : Maybe<WeatherModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weather: WeatherModel)

    @Update
    fun update(weather: WeatherModel)

    @Delete
    fun delete(weather: WeatherModel) : Int

    @Query("DELETE FROM WeatherModel")
    fun clearAll()
}