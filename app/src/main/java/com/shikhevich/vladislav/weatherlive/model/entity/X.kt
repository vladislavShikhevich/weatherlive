package com.shikhevich.vladislav.weatherlive.model.entity

import android.arch.persistence.room.*

@Entity
data class X(
    @Embedded var clouds: Clouds = Clouds(),
    var dt: Int = 0,
    var dt_txt: String = "",
    @Embedded var main: Main = Main(),
    @Embedded var rain: Rain = Rain(),
    @Embedded var sys: Sys = Sys(),
    @Ignore var weather: ArrayList<Weather> = ArrayList(),
    @Embedded var wind: Wind = Wind(),
    @PrimaryKey(autoGenerate = true) var idX: Int = 1
)