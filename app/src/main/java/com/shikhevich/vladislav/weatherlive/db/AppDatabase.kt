package com.shikhevich.vladislav.weatherlive.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.shikhevich.vladislav.weatherlive.db.dao.*
import com.shikhevich.vladislav.weatherlive.model.entity.*

@Database(entities = [
    Weather::class,
    WeatherModel::class,
    X::class],
    version = 1)
abstract class AppDatabase
    : RoomDatabase() {

    abstract fun weatherDao() : WeatherDao

    abstract fun weatherModelDao() : WeatherModelDao

    abstract fun xDao() : XDao
}